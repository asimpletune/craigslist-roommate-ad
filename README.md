# craigslist-roommate-ad
Write a CL ad in markdown, copy + paste it into CL.

## Instructions

1. Clone repo
2. Install dependencies
3. Start program and edit your ad away!

```bash
git clone git@github.com:asimpletune/craigslist-roommate-ad.git # Clone repo
cd craigslist-roommate-ad && npm install                        # Install dependencies
npm start && vim ad.md                                          # Start and edit
```
